var CSStransforms = anime({
  targets: '#mover .el',
  translateX: 150,
  scale: 2,
  rotate: '1turn'
});
/*animacion para blade imagenes decocasion*/
var specificPropertyParameters = anime({
  targets: '#rotar .ocasion',
  translateX: {
    value: 250,
    duration: 800
  },
  rotate: {
    value: 360,
    duration: 1800,
    easing: 'easeInOutSine'
  },
  scale: {
    value: 2,
    duration: 1600,
    delay: 800,
    easing: 'easeInOutQuart'
  },
  delay: 250 // All properties except 'scale' inherit 250ms delay
});

var alternate = anime({
  targets: '#regresar .imgdos',
  translateX: 250,
  direction: 'alternate'
});

var relativeValues = anime({
  targets: '#estirar .imgtres',
  translateX: {
    value: '+=150',
    duration: 1000
  },
  width: {
    value: '-=10',
    duration: 1800,
    easing: 'easeInOutSine'
  },
  height: {
    value: '*=4',
    duration: 1800,
    easing: 'easeInOutSine'
  },
  direction: 'alternate'
});