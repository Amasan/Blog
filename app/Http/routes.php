<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
/*la primera vista a mostrar sera el index de la pagina*/
Route::get('/', function () {
    /*return view('index');*/
    return redirect()->route('metztli.index');
});
//principal
Route::group(['prefix' => 'visitante'], function()
{
	Route::get('inicio', ['as' => 'metztli.index', 'uses' => 'Visitante\Home\HomeViewsController@Index']);
	Route::get('frases', ['as' => 'metztli.frases', 'uses'=> 'Visitante\Home\HomeViewsController@Frases']);
	Route::get('imagenes', ['as' => 'metztli.imagenocasion', 'uses'=> 'Visitante\Home\HomeViewsController@Imagen']);
});