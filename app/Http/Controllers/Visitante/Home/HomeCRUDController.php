<?php

namespace App\Http\Controllers\Contador\Home;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon as DateFormatter;
use App\Model\User;
use App\Model\PersonaFisica;
use App\Model\PersonaMoral;

class HomeCRUDController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        $this->middleware('role:partner');
    }

    /**
    * Buscador de contribuyentes por nombre fiscal y comercial (física y moral)
    * Url: contadorescorporativos/public/contabilidad/search/contribuyente
    * As: search.contribuyente
    * Petición: GET
    *@return View.
    */
    public function SearchContribuyente($search)
    {
        $data = Input::all();
        $PersonaFisica = PersonaFisica::Search($search);
        $PersonaMoral = PersonaMoral::Search($search);
        $count_query = count($PersonaFisica) + count($PersonaMoral);
        foreach ($PersonaMoral as $key => $value) {
            if(Auth::user()->login == "brenda@contadorescorporativos.com"){
                $value->visible = 0;
            }else{
                $value->visible = 1;
            }
        }
        return view('contador.home.index', compact('PersonaFisica','PersonaMoral'));
    }

}