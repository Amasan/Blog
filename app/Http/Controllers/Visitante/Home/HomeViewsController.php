<?php

namespace App\Http\Controllers\Visitante\Home;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Carbon\Carbon as DateFormatter;

class HomeViewsController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        //$this->middleware('role:partner');
    }

    /**
    * Método que muestra la vista de inicio de la aplicación.
    * Url: contadorescorporativos/public/Visitante/inicio
    * As: visitantes.partners
    * Petición: GET
    *@return View().
    */
    public function Index()
    {
        return view('visitante.home.index');
    }
    public function Frases()
    {
        return view('visitante.seccion.frases');
    }
    public function Imagen()
    {
        return view('visitante.seccion.imagenocasion');
    }
}