<footer class="page-footer font-small teal pt-4">
  <div class="container-fluid text-center text-md-left">
    <div class="row">
       <div class="col-md-6 mt-md-0 mt-3">
          <h3 class="text-uppercase font-weight-bold">Inspiración y contemplación</h3>
             <h6>Detenerse un momento para apreciar las maravillas de la naturaleza, expresar con palabras aquellos sentimientos, emociones que nacen.</h6>
        </div>
        <hr class="clearfix w-100 d-md-none pb-3">
        <div class="col-md-6 mb-md-0 mb-3">
          <h3 class="text-uppercase font-weight-bold">Redes sociales</h3>
          <h6>Disfruta de los diferentes pines, buscanos en Pinteres</h6>
        </div>
    </div>
  </div>
  <div class="footer-copyright text-center py-3">© 2018 Copyright:
    <a href="#">Amalia Santiago Marcos</a>
  </div>
</footer>