<div class="bordemenu">
  <nav class="navbar navbar-default">
    <div class="container-fluid texto-nav">
      <div class="navbar-header">
        <a class="conten-nav a-texto" href="{!! route('metztli.index') !!}">Inicio</a>
      </div>
      <div class="navbar-header">
        <a class="conten-nav a-texto" href="{!! route('metztli.frases') !!}">Frases</a>
      </div>
      <div class="navbar-header">
        <a class="conten-nav a-texto" href="{!! route('metztli.imagenocasion') !!}">Imagenes de ocasión</a>
      </div>
    </div>
  </nav>
</div>