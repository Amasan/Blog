<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es" lang="es" class="ve-animate">
<meta name="og:picture" content="/globalassets/toppbilder/korsbarsblommor-2017-2.png">
        <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE9">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1"><!--linea para que el tamaño de la pagina se ajuste al movil-->
        <!--link que carga los recursos de bootstrap-->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<head>
    <title>Metztli</title>
</head>
	<body class="detalles cuerpo">
        <div class="container-fluid">
            <div class="row">
                <div class="menu-principal">
            		@include('visitante.home.menu')
            	</div>
                <!-- menu principal, slider-->
                <div class="slider-principal">
            		@include('visitante.home.slider')
                </div>
                <!--cuerpo de la página-->
                <div class="galeria">
                        @include('visitante.home.galeria')
                </div>
                <div class="piepagina">
                    @include('visitante.home.footer')
                </div>
            </div>
        </div>
        <link rel="stylesheet" type="text/css" href="../css/slider.css">
        <link rel="stylesheet" type="text/css" href="../css/menu.css">
        <link rel="stylesheet" type="text/css" href="../css/galeria.css">
        <link rel="stylesheet" type="text/css" href="../css/anim.css">
        <link rel="stylesheet" type="text/css" href="../css/footer.css">
        <script src="../../node_modules/animejs/anime.js"></script><!--libreria de animación-->
        <script src="{{url()}}/js/efecto.js"></script>
        <script src="{{url()}}/js/anim.js"></script>
         <!--<script src="{{url()}}/js/particule.js"></script>-->
    </body>
</html>
