<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
        <div class="carousel-item active">
           <img class="imagen-principal" src="../../resources/imagen/slider/bluemoon.png">
        </div>
        <div class="carousel-item">
            <img class="imagen-principal" src="../../resources/imagen/slider/luna.jpg">
        </div>
        <div class="carousel-item">
            <img class="imagen-principal" src="../../resources/imagen/slider/moon.jpg">
        </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>
<!--
	para que el carrusel sea funcional es necesario cargar los recursos, para este en particular
	se agregaron las lineas 7 a 9 del archivo index, tomados de la pagina de bootstrap
</div>-->