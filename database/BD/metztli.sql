
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) NOT NULL,
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'Nombre o Nombres',
  `email` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL COMMENT 'correo electrónico con cual autenticarse en el sitio',
  `login` varchar(100) NOT NULL COMMENT 'Por el momento contendrá lo mismo que el email',
  `password` varchar(60) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'contraseña de la cuenta',
  `remember_token` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '2' COMMENT '0 = disabled; 1 = enabled; 2 =  pending approved',
  `created_at` timestamp NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'fecha de creacion',
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'fecha de modificacion',
  `user_group_id` bigint(20) NOT NULL COMMENT 'tipo de usuario',
  `telefono` varchar(15) NOT NULL COMMENT 'teléfono del usuario'
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `login`, `password`, `remember_token`, `status`, `created_at`, `updated_at`, `user_group_id`, `telefono`) VALUES
(1, 'Amalia Santiago Marcos', 'amalia@hotmail.com', 'amalia@hotmail.com', '1234', 'CBBPToIj4JIk0TiDH6Z2PcIDBZ2v2JaYOOHYYaUxFfq30VhsuxF3DE1YXOIP', 1, '2017-06-22 15:00:00', '2018-02-09 18:36:43', 1, '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_group`
--

CREATE TABLE IF NOT EXISTS `user_group` (
  `id` bigint(22) NOT NULL,
  `name` varchar(45) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(60) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `user_group`
--

INSERT INTO `user_group` (`id`, `name`, `description`) VALUES
(1, 'ADMINISTRADOR', 'Usuario administrador');

ALTER TABLE `users`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `user_group`
--
ALTER TABLE `user_group`
  MODIFY `id` bigint(22) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;

